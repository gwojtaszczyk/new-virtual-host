import os
import subprocess
import sys

__author__ = 'grzesiek'

host_alias = raw_input("Enter new virtual host alias: ")
project_path = raw_input("Enter project path: ")
if not os.path.exists(project_path):
    print "Path doesn't exist"
    sys.exit()

# config file
apache_config = "/etc/apache2/sites-available/"
apache_logs_path = "/var/log/apache2/"
new_config = host_alias + ".conf"
folders_name = host_alias.split('.')
folders_name = folders_name[0]
config = """<VirtualHost *:80>
	ServerName """ + host_alias + """
	ServerAlias www.""" + host_alias + """
	ServerAdmin webmaster@localhost
	DocumentRoot """ + project_path + """

	ErrorLog ${APACHE_LOG_DIR}/""" + folders_name + """/error.log
	CustomLog ${APACHE_LOG_DIR}/""" + folders_name + """/access.log combined
<Directory """ + project_path + """>
    Options Indexes FollowSymLinks
    AllowOverride All
    Require all granted
</Directory>
</VirtualHost>"""

if not os.path.isfile(apache_config + new_config):
    print "Creating config file ..."
    with open(apache_config + new_config, "w") as file:
        file.write(config)
        file.close()

    # Folders and paths
    print "Creating folders and paths ... "
    if not os.path.exists(apache_logs_path + folders_name):
        os.mkdir(apache_logs_path + folders_name)

    # Enabling virtualhost
    print "Enabling virtualhost"
    try:
        subprocess.call("a2ensite " + host_alias + ".conf", shell=True)
    except Exception:
        print "Couldn't enable new host"

    # Restarting apache
    print "Reloading apache"
    try:
        subprocess.call("service apache2 reload", shell=True)
    except Exception:
        print "Couldn't reload apache2"

    # Editing /etc/hosts
    with open("/etc/hosts") as hosts_file:
        content = hosts_file.read()
        hosts_file.close()

    with open("/etc/hosts", "w") as hosts_file:
        hosts_file.write("127.0.0.1 \t " + host_alias + "\n")
        hosts_file.write(content)
        hosts_file.close()


    print "OK\n"
else:
    print "Virtual Host already exists"
    print "END\n"


